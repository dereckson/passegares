#!/usr/bin/env python
# -*- coding: utf-8 -*-
import yaml
import os,sys
import urllib.request

tmpFileName = "data.csv"
entetesDonnees = ['idExterne','nom','exploitant','latitude','longitude','couleur','couleurEvolution','vCreation','vMaj','vSuppression']
regionsFichier = {}
metadonnes = 102
prochaineCouleur = -1

def getCouleur():
  global prochaineCouleur
  prochaineCouleur += 1
  if prochaineCouleur >= 8:
    prochaineCouleur %= 8
  return prochaineCouleur

def getCouleurEvolution(nom, couleur):
  couleurEvolution = (couleur + len(nom)) % 8
  if couleurEvolution == couleur:
    couleurEvolution = (couleur + 3) % 8
  return couleurEvolution

def getEntetes(ligne, correspondancesCol = None):
    posCol = {}
    champ = ligne.strip().split(';')
    i = 0
    for c in champ:
        if correspondancesCol == None :
            posCol[c] = i
        elif c in correspondancesCol.keys():
            posCol[correspondancesCol[c]] = i
        i += 1
    return posCol

def nextIdRegion(fRegions):
  idMaxRegion = 0
  i = 0
  posIdRegion = 0
  for ligne in fRegions.readlines():
    contenu = ligne.strip().split(';')
    if i == 0: #entête
      j = 0
      for c in contenu:
        if c == 'id':
          posIdRegion = j
          break
        j += 1
    else:
      if idMaxRegion < int(contenu[posIdRegion]):
        idMaxRegion = int(contenu[posIdRegion])
    i += 1
  return idMaxRegion + 1

def getAllGaresId(fGares):
  idExternes = []
  i = 0
  posId = 0
  for ligne in fGares.readlines():
    contenu = ligne.strip().split(';')
    if i == 0: #entête
      j = 0
      for c in contenu:
        if c == 'idExterne':
          posId = j
          break
        j += 1
    else:
      idExternes.append(contenu[posId])
    i += 1
  return idExternes

def getFichierRegion(gare, config):
    global metadonnes
    regionGare = gare['dossierId']
    if not regionGare in regionsFichier.keys(): #Pas encore en cache
        #On regarde si le dossier existe ou pas
        regionPath = '../app/src/main/assets/'+regionGare+'/'
        if not os.path.isdir(regionPath):
            #On crée le dossier
            os.mkdir(regionPath)
            #On crée les fichiers
            regionsFichier[regionGare] = {
                'gares': open(regionPath+'Gares.csv', 'a+'),
                'lignes': open(regionPath+'Lignes.csv', 'a'),
                'gdl': open(regionPath+'GaresDansLigne.csv', 'a'),
                'idGares': []
            }
            #Et on va leur mettre leur entêtes
            regionsFichier[regionGare]['gares'].write("idExterne;nom;exploitant;latitude;longitude;couleur;couleurEvolution;vCreation;vMaj;vSuppression\n")
            regionsFichier[regionGare]['lignes'].write("idExterne;nom;type;ordre;couleur;vCreation;vMaj;vSuppression\n")
            regionsFichier[regionGare]['gdl'].write("idGare;idLigne;ordre;PDLFond;PDLPoint;vCreation;vMaj;vSuppression\n")
            
            #Si ligne unique, on la crée a ce moment
            if not config['lignes']:
              if regionGare in config['substitue']['dossierId'].keys():
                regionGareAlt = config['substitue']['dossierId'][regionGare]
              else:
                regionGareAlt = regionGare
              regionsFichier[regionGareAlt]['lignes'].write(config['prefixIdExterne']+"_"+regionGareAlt+"_U;Ligne Unique;"+config['lignesType']+";0;#000000;"+str(metadonnes)+";0;0\n")
            #On rempli également le fichier des régions
            fRegions = open('../app/src/main/assets/Regions.csv', 'r+')
            #Il nous faut l'id max (dernière région)
            idNewRegion = nextIdRegion(fRegions)
            fRegions.write(str(idNewRegion)+";"+gare['region']+";"+regionGare+";"+str(metadonnes)+";0\n")
            fRegions.close()
        else:
            #On récupère les trois fichiers
            regionsFichier[regionGare] = {
                'gares': open(regionPath+'Gares.csv', 'r+'),
                'lignes': open(regionPath+'Lignes.csv', 'a'),
                'gdl': open(regionPath+'GaresDansLigne.csv', 'a')
            }
            #Et on génère le tableau idGares
            regionsFichier[regionGare]['idGares'] = getAllGaresId(regionsFichier[regionGare]['gares'])
        
        #On récupère également les entêtes
        regionsFichier[regionGare]['gares'].seek(0, 0)
        regionsFichier[regionGare]['enteteGare'] = getEntetes(regionsFichier[regionGare]['gares'].readline())
        #Et on remet le curseur à sa place
        regionsFichier[regionGare]['gares'].seek(0, 2)
        
    return regionsFichier[regionGare]

#On récupère le fichier de config
for filePath in os.listdir('config/'):
  fConfig = open("config/"+filePath, 'r+')
  config = yaml.load(fConfig)
  fConfig.close()

  #Étape 1 : Téléchargement du fichier
  lien = config['link_download']
  print('Téléchargement de : ' + lien)
  urllib.request.urlretrieve(lien, tmpFileName) #Désactivation durant les tests
  print('Téléchargement terminé')
  fData = open(tmpFileName, 'r+')

  #Étape 2 : On récupère ses entêtes
  titre = fData.readline()
  posCol = getEntetes(titre, config['header'])
  print('Liste des entêtes : ')
  print(posCol)

  #Étape 3 : On lit le fichier
  for ligne in fData.readlines():
    contenu = ligne.strip().split(';')
    gare = {}
    for c,i in posCol.items():
      gare[c] = contenu[i]
    
    #On va regarder la région
    regionGare = gare['dossierId']
    gare['region'] = regionGare
    #A-t-elle une valeur normalisée ?
    if regionGare in config['substitue']['dossierId'].keys():
      regionGare = config['substitue']['dossierId'][regionGare]
      gare['dossierId'] = regionGare
        
    if len(regionGare) > 0 and not regionGare in config['ignore_values']['dossierId']:
      #On ajoute le prefixe à l'Id
      gare['idExterne'] = config['prefixIdExterne']+gare['idExterne']
      
      if 'idExterne' in config['substitue'] and gare['idExterne'] in config['substitue']['idExterne']:
        gare['idExterne'] = str(config['substitue']['idExterne'][gare['idExterne']])
      #On récupère le fichier de la région
      fRegionInfos = getFichierRegion(gare, config)
      
      #On ne traite que les nouvelles gares
      if not gare['idExterne'] in fRegionInfos['idGares']:
        print('nouvelle gare : ' + gare['nom'])
        fRegionInfos['idGares'].append(gare['idExterne'])
        if not 'exploitant' in gare:
          gare['exploitant'] = config['default_value']['exploitant']
        #On va récupérer la couleur et la couleur évolution
        gare['couleur'] = getCouleur()
        gare['couleurEvolution'] = getCouleurEvolution(gare['nom'], gare['couleur'])
        #On rempli le fichier Gares
        fRegionInfos['gares'].write(gare['idExterne']+';'+gare['nom']+';'+gare['exploitant']+';'+gare['latitude']+';'+gare['longitude']+';'+str(gare['couleur'])+';'+str(gare['couleurEvolution'])+';'+str(metadonnes)+';0;0'+"\n")
      
      #On regarde pour la ligne maintenant
      if not config['lignes']: #Ligne unique
        regionsFichier[regionGare]['gdl'].write(gare['idExterne']+";"+config['prefixIdExterne']+"_"+regionGare+"_U;0;0;0;"+str(metadonnes)+";0;0\n")

  #Étape 4 : On ferme les fichiers
  for regionId,regionFichier in regionsFichier.items():
    regionFichier['gares'].close()
    regionFichier['lignes'].close()
    regionFichier['gdl'].close()
  
  #Étape 5 : On efface le fichier data
  os.remove(tmpFileName)

print('Fin de l\'import')
