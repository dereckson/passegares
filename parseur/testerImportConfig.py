import yaml
import os,sys

# Test de la validité yml des fichiers de config
for filePath in os.listdir('config'):
	fConfig = open("config/"+filePath, 'r+')
	yaml.load(fConfig)
	fConfig.close()

sys.exit(0)