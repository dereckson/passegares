package fr.nocle.passegares;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import fr.nocle.passegares.adapter.LigneTamponneeAdapter;
import fr.nocle.passegares.controlleur.TamponCtrl;
import fr.nocle.passegares.modele.LigneTamponnee;

public class ResumeVisaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume_visa);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();
        final boolean voirTamponDuJour = i.getBooleanExtra("DUJOUR", false);

        if(voirTamponDuJour)
            setTitle(R.string.titre_tampons_du_jour);
        else
            setTitle(R.string.titre_tampons);

        TamponCtrl tamponControlleur = new TamponCtrl(this);
        final ArrayList<LigneTamponnee> listeLignes = tamponControlleur.getByLines(voirTamponDuJour);

        Collections.sort(listeLignes, new Comparator<LigneTamponnee>() {
            @Override
            public int compare(LigneTamponnee o1, LigneTamponnee o2) {
                if(o1.getOrdre() > o2.getOrdre())
                    return 1;
                else if(o1.getOrdre() < o2.getOrdre())
                    return -1;
                else
                    return 0;
            }
        });

        // Create the adapter to convert the array to views
        LigneTamponneeAdapter adapter = new LigneTamponneeAdapter(this, listeLignes, voirTamponDuJour);

        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.listeLignes);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LigneTamponnee ligneTamponnee = listeLignes.get(position);
                Intent i = new Intent(getApplicationContext(), VisaActivity.class);
                i.putExtra("IDLIGNE", ligneTamponnee.getIdLigne());
                if(voirTamponDuJour)
                    i.putExtra("DUJOUR", true);
                startActivity(i);
            }
        });
        tamponControlleur.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tampon, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.voirTout:
                Intent i = new Intent(getApplicationContext(), VisaActivity.class);
                i.putExtra("IDLIGNE", 0);

                Intent intentParent = getIntent();
                boolean voirTamponDuJour = intentParent.getBooleanExtra("DUJOUR", false);
                if(voirTamponDuJour)
                    i.putExtra("DUJOUR", true);
                startActivity(i);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
