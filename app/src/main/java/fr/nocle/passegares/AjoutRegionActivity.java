package fr.nocle.passegares;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import fr.nocle.passegares.adapter.RegionsAdapter;
import fr.nocle.passegares.controlleur.ImportCSV;
import fr.nocle.passegares.controlleur.RegionCtrl;
import fr.nocle.passegares.modele.Region;

class AjoutRegionThread extends Thread {
    private Context contexte;
    private DialogInterface loadingDialog;
    private Region region;
    private boolean installation;

    AjoutRegionThread(Context context, DialogInterface loadingDialog, Region region, boolean installation) {
        this.contexte = context;
        this.loadingDialog = loadingDialog;
        this.region = region;
        this.installation = installation;
    }

    public void run() {
        RegionCtrl regionCtrl = new RegionCtrl(contexte);
        SQLiteDatabase bdd = regionCtrl.open();

        //On utilise une transaction pour que tout passe en même temps
        bdd.beginTransaction();
        ImportCSV.updateAllDataRegion(contexte, bdd, 1, -1, region);

        //Et on met à jour la région !
        region.setEstInstalle(true);
        regionCtrl.update(region);

        bdd.setTransactionSuccessful();
        bdd.endTransaction();
        regionCtrl.close();

        loadingDialog.dismiss();
    }
}

public class AjoutRegionActivity extends AppCompatActivity {
    private RegionCtrl regionCtrl;
    private boolean installation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_region);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();
        installation = i.getBooleanExtra("INSTALLATION", false);

        if(installation)
        {
            TextView label = (TextView) findViewById(R.id.label);
            label.setText(R.string.premierDemarrageChoixRegion);
        }

        regionCtrl = new RegionCtrl(this);
        regionCtrl.open();
        final ArrayList<Region> listeRegions = regionCtrl.getAllRegions(RegionCtrl.TOUTESREGIONS);

        RegionsAdapter adapter = new RegionsAdapter(this, listeRegions);
        ListView listView = (ListView) findViewById(R.id.listeRegions);
        listView.setAdapter(adapter);

        //À l'acceptation, on affiche une boîte de dialogue de confirmation
        final AlertDialog.Builder dialogInstallRegion = new AlertDialog.Builder(this);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, int position, long id) {
                if(view.isEnabled()) {
                    final Region region = listeRegions.get(position);

                    if(installation) //Pas de dialog, on installe direct !
                    {
                        lancerInstallation(region, view);
                    } else {
                        dialogInstallRegion.setMessage(R.string.dialogAjoutRegionExplication).setTitle(R.string.dialogAjoutRegionTitre);
                        dialogInstallRegion.setPositiveButton(R.string.boutonValider, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // On valide l'installation des nouvelles données
                                dialog.cancel();
                                lancerInstallation(region, view);
                            }
                        });
                        dialogInstallRegion.setNegativeButton(R.string.boutonAnnuler, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // On refuse l'installation des données
                                dialog.cancel();
                            }
                        });

                        dialogInstallRegion.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                parent.invalidate(); //Marche pas
                            }
                        });

                        dialogInstallRegion.create();
                        dialogInstallRegion.show();
                    }
                }
            }
        });
    }

    private void lancerInstallation(Region region, View view)
    {
        ProgressDialog loadingDialog = ProgressDialog.show(AjoutRegionActivity.this, null, AjoutRegionActivity.this.getString(R.string.dialogAjoutRegionEnCours), true);
        AjoutRegionThread p = new AjoutRegionThread(AjoutRegionActivity.this, loadingDialog, region, installation);
        p.start();
        CheckBox checkBox = (CheckBox) view.findViewById(R.id.estInstalle);
        checkBox.setChecked(true);
        view.setEnabled(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        regionCtrl.close();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
