package fr.nocle.passegares.outils;

import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by jonathanmm on 11/12/16.
 */

public class StringOutils
{
    public static String displayBeautifullNameStation(String name, int limit)
    {
        String[] mots = name.split(" ");
        ArrayList<String> motsFormate = new ArrayList<>();
        boolean premierMot = true;
        ArrayList<String> particulesNonMajuscule = new ArrayList<String>();
        particulesNonMajuscule.add("de");
        particulesNonMajuscule.add("du");
        particulesNonMajuscule.add("la");
        particulesNonMajuscule.add("le");
        particulesNonMajuscule.add("et");
        particulesNonMajuscule.add("au");
        particulesNonMajuscule.add("a");
        particulesNonMajuscule.add("sur");
        particulesNonMajuscule.add("sous");
        for(String mot : mots)
        {
            String[] composants = mot.split("-");
            ArrayList<String> bouts = new ArrayList<>();
            for(String composant : composants)
            {
                String motFormate = "";
                int posPremiereLettre = 0;

                if(composant.substring(0, 1).equals("("))
                {
                    motFormate = "(";
                    posPremiereLettre = 1;
                } else if(composant.length() >= 2 && (composant.substring(1, 2).equals("'") || composant.substring(1, 2).equals("’")))
                {
                    if(premierMot)
                        motFormate = composant.substring(0, 1).toUpperCase() + "'";
                    else
                        motFormate = composant.substring(0, 1).toLowerCase() + "'";
                    posPremiereLettre = 2;
                }

                if(premierMot || !particulesNonMajuscule.contains(composant.substring(posPremiereLettre).toLowerCase()))
                    motFormate = motFormate + composant.substring(posPremiereLettre, posPremiereLettre + 1).toUpperCase() + composant.substring(posPremiereLettre + 1).toLowerCase();
                else
                    motFormate = motFormate + composant.substring(posPremiereLettre).toLowerCase();

                bouts.add(motFormate);

                if(premierMot)
                    premierMot = false;
            }
            motsFormate.add(TextUtils.join("-", bouts));
        }
        String phraseFormate = TextUtils.join(" ", motsFormate);
        if(limit != 0 && phraseFormate.length() > limit)
            return phraseFormate.substring(0, limit) + "…";
        else
            return phraseFormate;
    }

    public static String displayBeautifullNameStation(String name)
    {
        return displayBeautifullNameStation(name, 0);
    }

    public static String getRelativeDate(Date dateAbsolue)
    {
        if(dateAbsolue == null)
            return "";
        Date auj = new Date();
        long millisecondes = auj.getTime() - dateAbsolue.getTime();
        int secondes = Math.round(millisecondes / 1000);
        if(secondes < 2)
            return "Il y a "+secondes+" seconde";
        else if(secondes < 60)
            return "Il y a "+secondes+" secondes";
        else if(secondes < 120)
        {
            int sec = secondes % 60;
            return "Il y a 1 minute"+(sec > 0 ? " " + putZero(sec) + " sec" : "");
        }
        else if(secondes < 3600)
        {
            int sec = secondes % 60;
            return "Il y a "+(int) Math.ceil(secondes/60)+" minutes"+(sec > 0 ? " " + putZero(sec)+" sec" : "");
        }
        else if(secondes < 7200)
        {
            int min = (int) Math.ceil(secondes / 60) % 60;
            return "Il y a 1 heure" + (min > 0 ? " "+putZero(min) + " min" : "");
        }
        else if(secondes < 86400)
        {
            int min = (int) Math.ceil(secondes / 60) % 60;
            return "Il y a "+(int) Math.ceil(secondes/3600)+" heures" +(min > 0 ? " "+putZero(Math.ceil(secondes / 60) % 60) + " min" : "");
        }
        else if(secondes < 172800) //86400*2
        {
            int h = (int) Math.ceil(secondes/3600) % 24;
            return "Il y a 1 jour" + (h > 0 ? ", "+h+"h"+putZero(Math.ceil(secondes / 60) % 60) : "");
        }
        else if(secondes <= 1209600) //86400*14
        {
            int h = (int) Math.ceil(secondes/3600) % 24;
            return "Il y a "+(int) Math.ceil(secondes/86400)+" jours" + (h > 0 ? ", "+h+"h"+putZero(Math.ceil(secondes / 60) % 60) : "");
        }
        else
        {
            SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy HH'h'mm");
            return formatDate.format(dateAbsolue);
        }
    }

    private static String putZero(int nombre)
    {
        if(nombre <= 9)
            return "0"+String.valueOf(nombre);
        else
            return String.valueOf(nombre);
    }

    private static String putZero(double nombre)
    {
        return putZero((int) nombre);
    }
}
