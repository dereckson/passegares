package fr.nocle.passegares;

import android.text.TextUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import fr.nocle.passegares.outils.StringOutils;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

/**
 * Created by jonathanmm on 08/01/17.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest(TextUtils.class)
public class StringOutilsTest {
    @Before
    public void initMock() {
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.join(anyString(), any(ArrayList.class))).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                String delimiter = (String) invocation.getArguments()[0];
                ArrayList<String> bouts = (ArrayList<String>) invocation.getArguments()[1];

                if(bouts == null)
                    return "";

                boolean premier = true;
                String retour = "";
                for(String mot : bouts)
                {
                    if(!premier)
                        retour += delimiter;
                    else
                        premier = false;

                    retour += mot;
                }

                return retour;
            }
        });
    }

//DisplayBeautifulNameStation
    @Test
    public void displayBeautifullNameStation_unMot_formattageCorrect() {
        assertEquals("Bonhomme", StringOutils.displayBeautifullNameStation("BONHOMME"));
    }

    @Test
    public void displayBeautifullNameStation_troisMot_formattageCorrect() {
        assertEquals("Bonhomme de Neige", StringOutils.displayBeautifullNameStation("BONHOMME DE NEIGE"));
    }

    @Test
    public void displayBeautifullNameStation_troisMotTiret_formattageCorrect() {
        assertEquals("Bonhomme-de-Neige", StringOutils.displayBeautifullNameStation("BONHOMME-DE-NEIGE"));
    }

    @Test
    public void displayBeautifullNameStation_troisMotEspaceTiret_formattageCorrect() {
        assertEquals("Bonhomme de-Neige", StringOutils.displayBeautifullNameStation("BONHOMME DE-NEIGE"));
    }

    @Test
    public void displayBeautifullNameStation_longTitreCoupe_formattageCorrect() {
        assertEquals("Je Suis Un Titre de Plus de 30…", StringOutils.displayBeautifullNameStation("JE SUIS UN TITRE DE PLUS DE 30 CARACTÈRES !", 30));
    }

    @Test
    public void displayBeautifullNameStation_longTitreCoupeDix_formattageCorrect() {
        assertEquals("Je Suis Un…", StringOutils.displayBeautifullNameStation("JE SUIS UN TITRE DE PLUS DE 10 CARACTÈRES !", 10));
    }

    @Test
    public void displayBeautifullNameStation_troisMotParentheses_formattageCorrect() {
        assertEquals("Bonhomme (Neige)", StringOutils.displayBeautifullNameStation("BONHOMME (NEIGE)"));
    }

    @Test
    public void displayBeautifullNameStation_troisMotParenthesesLiaisons_formattageCorrect() {
        assertEquals("Bonhomme (de Neige)", StringOutils.displayBeautifullNameStation("BONHOMME (DE NEIGE)"));
    }

    @Test
    public void displayBeautifullNameStation_troisMotApostrophe_formattageCorrect() {
        assertEquals("Banane de l'Autruche", StringOutils.displayBeautifullNameStation("BANANE DE L'AUTRUCHE"));
    }

    @Test
    public void displayBeautifullNameStation_premierMotApostropheLiaison_formattageCorrect() {
        assertEquals("L'Autruche", StringOutils.displayBeautifullNameStation("L'AUTRUCHE"));
    }

    @Test
    public void displayBeautifullNameStation_casReel_formattageCorrect() {
        assertEquals("Esplanade de la Défense", StringOutils.displayBeautifullNameStation("ESPLANADE DE LA DÉFENSE"));
        assertEquals("Porte de Neuilly (Avenue de Madrid)", StringOutils.displayBeautifullNameStation("PORTE DE NEUILLY (AVENUE DE MADRID)"));
        assertEquals("Arts et Métiers", StringOutils.displayBeautifullNameStation("ARTS ET MÉTIERS"));
        assertEquals("Château d'Eau", StringOutils.displayBeautifullNameStation("CHÂTEAU D'EAU"));
        assertEquals("Gare de l'Est", StringOutils.displayBeautifullNameStation("GARE DE L'EST"));
        assertEquals("La Chapelle", StringOutils.displayBeautifullNameStation("LA CHAPELLE"));
        assertEquals("Asnières-sur-Seine", StringOutils.displayBeautifullNameStation("ASNIÈRES-SUR-SEINE"));
        assertEquals("Soisy-sous-Mortmorency", StringOutils.displayBeautifullNameStation("SOISY-SOUS-MORTMORENCY"));
    }

// getRelativeDate
    @Test
    public void getRelativeDate_null_formattageCorrect() {
        assertEquals("", StringOutils.getRelativeDate(null));
    }

    @Test
    public void getRelativeDate_maintenant_formattageCorrect() {
        assertEquals("Il y a 0 seconde", StringOutils.getRelativeDate(new Date()));
    }

    @Test
    public void getRelativeDate_cinqSecondes_formattageCorrect() {
        assertEquals("Il y a 5 secondes", StringOutils.getRelativeDate(new Date((new Date()).getTime() - 5000)));
    }

    @Test
    public void getRelativeDate_uneMinute_formattageCorrect() {
        assertEquals("Il y a 1 minute", StringOutils.getRelativeDate(new Date((new Date()).getTime() - 60000)));
    }

    @Test
    public void getRelativeDate_uneMinuteCinq_formattageCorrect() {
        assertEquals("Il y a 1 minute 05 sec", StringOutils.getRelativeDate(new Date((new Date()).getTime() - 65000)));
    }

    @Test
    public void getRelativeDate_cinqMinutes_formattageCorrect() {
        assertEquals("Il y a 5 minutes", StringOutils.getRelativeDate(new Date((new Date()).getTime() - 300000))); //1000 * 60 * 5
    }

    @Test
    public void getRelativeDate_cinqMinutesCinq_formattageCorrect() {
        assertEquals("Il y a 5 minutes 05 sec", StringOutils.getRelativeDate(new Date((new Date()).getTime() - 305000))); //1000 * 60 * 5
    }

    @Test
    public void getRelativeDate_uneHeure_formattageCorrect() {
        assertEquals("Il y a 1 heure", StringOutils.getRelativeDate(new Date((new Date()).getTime() - 3600000))); //1000 * 60 * 60
    }

    @Test
    public void getRelativeDate_uneHeureCinq_formattageCorrect() {
        assertEquals("Il y a 1 heure 05 min", StringOutils.getRelativeDate(new Date((new Date()).getTime() - 3900000))); //1000 * 60 * 60
    }

    @Test
    public void getRelativeDate_cinqHeures_formattageCorrect() {
        assertEquals("Il y a 5 heures", StringOutils.getRelativeDate(new Date((new Date()).getTime() - 18000000))); //1000 * 60 * 60 * 5
    }

    @Test
    public void getRelativeDate_cinqHeurescinq_formattageCorrect() {
        assertEquals("Il y a 5 heures 05 min", StringOutils.getRelativeDate(new Date((new Date()).getTime() - 18300000))); //1000 * 60 * 60 * 5
    }

    @Test
    public void getRelativeDate_unjour_formattageCorrect() {
        assertEquals("Il y a 1 jour", StringOutils.getRelativeDate(new Date((new Date()).getTime() - 86400000))); //1000 * 60 * 60 * 24
    }

    @Test
    public void getRelativeDate_unjourUneHeureDix_formattageCorrect() {
        assertEquals("Il y a 1 jour, 1h10", StringOutils.getRelativeDate(new Date((new Date()).getTime() - 90600000))); //1000 * 60 * 60 * 24
    }

    @Test
    public void getRelativeDate_cinqjour_formattageCorrect() {
        assertEquals("Il y a 5 jours", StringOutils.getRelativeDate(new Date((new Date()).getTime() - 432000000))); //1000 * 60 * 60 * 24 * 5
    }

    @Test
    public void getRelativeDate_cinqjourUneHeureUne_formattageCorrect() {
        assertEquals("Il y a 5 jours, 1h01", StringOutils.getRelativeDate(new Date((new Date()).getTime() - 435660000))); //1000 * 60 * 60 * 24 * 5
    }

    @Test
    public void getRelativeDate_ancien_formattageCorrect() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2007);
        cal.set(Calendar.MONTH, Calendar.FEBRUARY);
        cal.set(Calendar.DATE, 14); //Commence à 0 et non 1…
        cal.set(Calendar.HOUR, 23);
        cal.set(Calendar.MINUTE, 37);
        cal.set(Calendar.SECOND, 25);
        assertEquals("15/02/2007 11h37", StringOutils.getRelativeDate(cal.getTime()));
    }
}
